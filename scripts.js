var commonLink = 'https://widgets-dev.azurewebsites.net';
var api = commonLink + '';
ko.components.register('dashboard-grid', {
    viewModel: {
        createViewModel: function (controller, componentInfo) {
            var ViewModel = function (controller, componentInfo) {
                var grid = null;
                this.widgets = controller.widgets;
                this.afterAddWidget = function (items) {
                    if (grid == null) {
                        grid = $(componentInfo.element).find('.grid-stack').gridstack({
                            float: true,
                            auto: false,
                            disableResize: true,
                            disableDrag: !!localStorage.onlyView || false
                        }).data('gridstack');
                    }
                    var item = _.find(items, function (i) { return i.nodeType == 1 });
                    grid.addWidget(item);
                    ko.utils.domNodeDisposal.addDisposeCallback(item, function () {
                        grid.removeWidget(item);
                    });
                    if (!!localStorage.onlyView) {
                        $('.remove-btn').css('visibility', 'hidden');
                    }
                };
            };
            return new ViewModel(controller, componentInfo);
        }
    },
    template:
        [
            '<div class="grid-stack" data-bind="foreach: {data: widgets, afterRender: afterAddWidget}">',
            '   <div class="grid-stack-item" data-bind="attr: {\'data-gs-x\': $data.x, \'data-gs-y\': $data.y, \'data-gs-width\': $data.width, \'data-gs-height\': $data.height, \'data-gs-auto-position\': $data.auto_position,\'data-gs-url\': $data.url,\'data-gs-id\': $data.id,\'data-gs-widgetId\': $data.widgetId }">',
            '       <div class="grid-stack-item-content">' +
            '          <button class="btn btn-default remove-btn" data-bind="click: $root.deleteWidget">X</button>' +
                '<iframe data-bind="attr: {\'src\': $data.urlFull}" frameborder="0" class="modal-iframe"></iframe>'+
            '       </div>',
            '   </div>',
            '</div> '
        ].join('')
});

$(function () {
    var Controller = function (widgets) {
        var self = this;
        this.widgets = ko.observableArray(widgets);

        this.items = [];
        this.openPopup = function () {
            var selectedVal = $( "#widgetSelect" ).val();
            var findItem = self.items.find(function(item){
                return item.metadata.id == selectedVal;
            });

            var link = commonLink + "/" + findItem.metadata.configurator.url + "?token=" + localStorage.getItem('user_token');

            var pop_up = $('<div class="modal">' +
                '<div class="modal-dialog" style="max-width: '+ findItem.metadata.configurator.size.width +'px;max-height: '+ findItem.metadata.configurator.size.height + 'px;">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<h3 class="modal-title">Widget configuration</h3>' +
                '<button type="button" class="close">' +
                '<span>&times;</span>' +
                '</button>' +
                '</div>' +
                '<div class="modal-body">' +
                '<iframe frameborder="0" src="' + link + '" class="modal-iframe">' +
                '</iframe>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
            $(pop_up).appendTo('body');
            $(pop_up).show();

            $('.close').click(function () {
                $('.modal').fadeOut().remove();
                $(this).parent().fadeOut().remove();
                updateDashboard();
            });
        };

        this.savePosition = function(){
            var serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                el = $(el);
                return {
                    position: {
                        x: el.data('gsX'),
                        y: el.data('gsY')
                    },
                    size: {
                        width: el.data('gsWidth'),
                        height: el.data('gsHeight')
                    },
                    url: el.data('gsUrl'),
                    widgetId: el.data('gsWidgetid'),
                    id: el.data('gsId')
                };
            });

            $.ajax({
                type: 'PUT',
                url: commonLink + '/dashboard',
                contentType: "application/json",
                headers: {"Authorization": 'Bearer ' + localStorage.getItem('user_token')},
                data: JSON.stringify({widgets:serializedData}),
                success: function(data){
                    alert('saved')
                },
                error: function (data) {
                    if (data.status == 401) {
                        logout();
                    }
                }
            });
        };

        this.addNewWidget = function (obj) {
            if(obj){
                self.widgets.push({
                    x: obj.position.x || 0,
                    y: obj.position.y || 0,
                    width: obj.size.width,
                    height: obj.size.height,
                    url: obj.url,
                    urlFull: commonLink + obj.url,
                    widgetId: obj.widgetId,
                    id: obj.id
                });
            }
            return false;
        };

        this.deleteWidget = function (item) {
            self.widgets.remove(item);
            return false;
        };
    };
    var widgets = [];
    var controller = new Controller(widgets);
    ko.applyBindings(controller);

    if (localStorage.user_token) {
        $('.widget-section').show();
        $('.login-section').hide();
        getWidgetsList();
    } else {
        $('.widget-section').hide();
        $('.login-section').show();

        $('form').on('submit', function(e) {
            e.preventDefault();
            var login = $('#login').val();
            var password = $('#password').val();

            var loginData = {
                "userName": login,
                "password": password
            };
            if (login && password) {
                $.ajax({
                    type: 'POST',
                    url: api + '/tokens',
                    contentType: "application/json",
                    dataType: 'json',
                    headers: {"Access-Control-Allow-Origin": "*"},
                    data: JSON.stringify(loginData),
                    success: function (data) {
                        localStorage.user_token = data.accessToken;
                        getWidgetsList();
                    }
                });
            } else {
                alert('Please enter your login and password')
            }

        });
    }

    function updateDashboard(){
        controller.widgets.removeAll()
        $.ajax({
            type: 'GET',
            url: commonLink + '/dashboard',
            contentType: "application/json",
            headers: {"Authorization": 'Bearer ' + localStorage.getItem('user_token')},
            success: function(data){
                $.each(data.widgets, function (i, item) {
                    controller.addNewWidget(item);
                });
            },
            error: function (data) {
                if(data.status == 401){
                    logout();
                }
            }
        });
    }

    $('.logout').click(logout);

    function logout() {
        localStorage.clear();
        $('.widget-section').hide();
        $('.login-section').show();
    }

    function getWidgetsList(){
        $('.widget-section').show();
        $('.login-section').hide();

        if (localStorage.onlyView) {
            $('#onlyViewToggle').prop('checked', true);
            $('.widget-select').hide();
        } else {
            $('#onlyViewToggle').prop('checked', false);
            $('.widget-select').show();
        }

        $.ajax({
            type: 'GET',
            url: commonLink + '/widgets',
            contentType: "application/json",
            headers: {"Authorization": 'Bearer ' + localStorage.getItem('user_token')},
            success: function (data) {
                controller.items = data;

                $.each(data, function (i, item) {
                    $('#widgetSelect').append($('<option>', {
                        value: item.metadata.id,
                        text: item.metadata.name
                    }));
                });
            },
            error: function (data) {
                if (data.status == 401) {
                    logout();
                }
            }
        });

        updateDashboard();
    }

    $('#onlyViewToggle').on('click', function () {
        localStorage.onlyView = $('#onlyViewToggle').is(':checked') || '';
        location.reload();
    });
});