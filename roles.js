var commonLink = 'https://widgets-dev.azurewebsites.net';
var api = commonLink + '';

$(function () {
    var _this = this;
    var activeItemID;
    $.ajax({
        type: 'GET',
        url: commonLink + '/widgets',
        contentType: "application/json",
        headers: {"Authorization": 'Bearer ' + localStorage.getItem('user_token')},
        success: function (data) {
            _this.data = data;
            populateWidgets(data);
        },
        error: function (data) {
        }
    });

    $.ajax({
        type: 'GET',
        url: commonLink + '/roles',
        contentType: "application/json",
        success: function (data) {
            _this.roles = data;
            populateRoles(data);
        },
        error: function (data) {
        }
    });

    function populateRoles(roles) {
        $.each(roles, function (i, item) {
            $('.list-group.roles').append($('<li class="list-group-item">' + item.name + '<input type="checkbox" data-roleId="' + item.id + '" data-roleName="'+ item.name +'" class="roles-checkbox float-right"></li>'));
        });

        $('.roles-checkbox').click(function (e) {
            e.stopPropagation();
            var foundItem = findItem(activeItemID);
            if (foundItem) {
                var index;
                    foundItem.availableRoles.find(function(item, i){
                    if(e.currentTarget.dataset.roleid === item.id){
                        index = i;
                        return item;
                    }
                });
                if (index > -1) {
                    foundItem.availableRoles.splice(index, 1);
                } else {
                    foundItem.availableRoles.push({id: e.currentTarget.dataset.roleid, name: e.currentTarget.dataset.roleName });
                }
            }
        });
    }

    function populateWidgets(data) {
        $.each(data, function (i, item) {
            $('.list-group.widgets').append($('<li class="list-group-item" data-id="' + item.metadata.id + '">' + item.metadata.name + '<input type="checkbox" class="list-item__checkbox float-right" ' + (!item.isEnabled || "checked") + ' data-id="' + item.metadata.id + '"></li>'));
        });

        $('.list-item__checkbox').click(function (e) {
            e.stopPropagation();
            var foundItem = findItem(e.currentTarget.dataset.id);

            if (foundItem) {
                foundItem.isEnabled = !foundItem.isEnabled;

                $(e.currentTarget).prop('checked', foundItem.isEnabled);
            }
        });

        $('.widgets .list-group-item').click(function (e) {
            e.preventDefault();
            $('.list-group-item').removeClass('active');
            $(e.currentTarget).addClass('active');

            var foundItem = findItem(e.currentTarget.dataset.id);
            activeItemID = foundItem.metadata.id;
            populateAvailableRoles(foundItem.availableRoles);
        });
    }

    function populateAvailableRoles(availableRoles) {
        $('.list-group.roles .list-group-item input').each(function (i, item) {
            var faundRole = availableRoles.find(function(role){
                return item.dataset.roleid === role.id;
            });

            if (faundRole) {
                $(item).prop('checked', true);
            } else {
                $(item).prop('checked', false);
            }
        });
    }

    function findItem(id) {
        return _this.data.find(function (item) {
            return item.metadata.id == id;
        });
    }

    $('.save').click(function (e) {
        $.ajax({
            type: 'PUT',
            url: commonLink + '/widgets',
            contentType: "application/json",
            headers: {"Authorization": 'Bearer ' + localStorage.getItem('user_token')},
            data: JSON.stringify(_this.data),
            success: function(data){
                alert('saved')
            },
            error: function (data) {}
        });
    })
});